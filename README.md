# About

That's a usual TTRSS installation, plus feedly theme (font was modified).

# Available environment variables:
- `TT_REFRESH` (default is 5) - interval in minutes between feeds updates
- `TT_DB_HOST` (default is db) - MySQL hostname to connect
- `TT_DB_NAME` (default is ttrss) - MySQL database name
- `TT_DB_USER` (default is root) - MySQL username
- `TT_DB_PASS` (default is ttrss) - MySQL password
- `TT_DOMAIN` - end user domain name to be placed in ttrss `config.php`

# How to launch
## pull the images
```
docker pull alamut/ttrss:latest
docker pull  mysql:5.5
```
## then start containers
```
docker run --name mysql_ttrss -v /opt/docker_data/mysql_ttrss:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=ttrss -d mysql:5.5
docker run -d --link mysql_ttrss:db -p 80:80 -e TT_DOMAIN=example.com --name=ttrss alamut/ttrss
```
## in case of upstart installed, a couple of service defs
_/etc/init/mysql_ttrss.conf_

```
start on filesystem and started docker
stop on runlevel [!2345]
respawn
exec /usr/bin/docker start -a mysql_ttrss
```
_/etc/init/ttrss.conf_

```
start on filesystem and started docker
stop on runlevel [!2345]
respawn
exec /usr/bin/docker start -a ttrss
```
